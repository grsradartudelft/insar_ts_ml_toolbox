import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import utils.PLR_segment.segment as segment
import utils.PLR_segment.fit as fit
import utils.PLR_segment.wrappers as wrappers


def PLR_seg_reg(data, max_error=500):
    # sliding window with regression
    try:
        sw_segments = segment.slidingwindowsegment(data, fit.regression, fit.sumsquared_error, max_error)
        sw_seg = [seg[2] for seg in sw_segments[:-1]]
    except ValueError:
        sw_segments = []
        sw_seg = []

    # bottom-up with regression
    try:
        bu_segments = segment.bottomupsegment(data, fit.regression, fit.sumsquared_error, max_error)
        bu_seg = [seg[2] for seg in bu_segments[:-1]]
    except ValueError:
        bu_segments = []
        bu_seg = []

    # top-down with regression
    try:
        td_segments = segment.topdownsegment(data, fit.regression, fit.sumsquared_error, max_error)
        td_seg = [seg[2] for seg in td_segments[:-1]]
    except ValueError:
        td_segments = []
        td_seg = []

    segs = [sw_seg, bu_seg, td_seg]

    segments = [sw_segments, bu_segments, td_segments]

    return segments, segs


def PLR_seg_inter(data, max_error=500):
    # sliding window with regression
    try:
        sw_segments = segment.slidingwindowsegment(data, fit.interpolate, fit.sumsquared_error, max_error)
        sw_seg = [seg[2] for seg in sw_segments[:-1]]
    except ValueError:
        sw_segments = []
        sw_seg = []

    # bottom-up with regression
    try:
        bu_segments = segment.bottomupsegment(data, fit.interpolate, fit.sumsquared_error, max_error)
        bu_seg = [seg[2] for seg in bu_segments[:-1]]
    except ValueError:
        bu_segments = []
        bu_seg = []

    # top-down with regression
    try:
        td_segments = segment.topdownsegment(data, fit.interpolate, fit.sumsquared_error, max_error)
        td_seg = [seg[2] for seg in td_segments[:-1]]
    except ValueError:
        td_segments = []
        td_seg = []

    segs = [sw_seg, bu_seg, td_seg]

    segments = [sw_segments, bu_segments, td_segments]

    return segments, segs


def draw_segments(segments):
    ax = plt.gca()
    for segment in segments:
        #         line = Line2D((indices[segment[0]],indices[segment[2]]),(segment[1],segment[3]), color = "grey")    # For T-S w/ MO
        line = Line2D((segment[0], segment[2]), (segment[1], segment[3]), color="grey")  # For T-S w/o MO
        ax.add_line(line)