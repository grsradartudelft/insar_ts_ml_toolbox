import numpy as np

def checkPerformance(pred, groundTruth, tolerance=2):
    '''
    Computes recall and precision scores for time-series segmentation predictions.
    Detected segmentation line is allowed to be +/- tolerance from gorund truth line.
    
    Arguments:
    pred - predicted segmentation lines
    groundTruth - ground truth segmentation lines
    tolerance - how far predicted line can be from ground truth line to be considered as true positive
    
    Returns:
    recall
    precision
    '''

    pred = set(pred)
    groundTruth = set(groundTruth)
    tp = 0
    fp = 0
    fn = 0
    
    for i in pred:
        temp = set([i])
        for j in range(1, tolerance+1):
            temp.add(i+j)
            temp.add(i-j)

        if bool(temp.intersection(groundTruth)):
            tp = tp + 1
            groundTruth = groundTruth - temp.intersection(groundTruth)
        else:
            fp = fp + 1
            
    for i in groundTruth:
        fn = fn + 1
    
    if tp or fn:
        recall = tp / (tp +fn)
    else:
        recall = np.nan
        
    if tp or fp:
        precision = tp / (tp+fp)
    else:
        precision = np.nan
        
        
        
    return recall, precision, tp, fp, fn
