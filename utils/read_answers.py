import ast
import pandas as pd


def ReadAnswers(filename):
    ans = []

    r = []
    with open(filename) as fin:
        for _ in range(2):
            next(fin)
        for line in fin:
            row = line.rstrip('\n,').lstrip('{').partition(':')
            r.append(row)

        data = {rr[0]: ast.literal_eval(rr[2]) for rr in r}
    
    df = pd.DataFrame.from_dict(data, orient="index")
    df = df.reset_index().set_index('ts_num')
    df.sort_index(inplace=True)
    df = df.rename(index=str, columns={"index": "point_id"})
    df.index = df.index.astype(int, copy=False)
    
    return df
