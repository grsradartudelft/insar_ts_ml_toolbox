import pandas as pd
import numpy as np
from statsmodels.tsa.arima_model import ARMA
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.holtwinters import ExponentialSmoothing, SimpleExpSmoothing


def ARIMAForecast(ts_num, data):

    yhat = None
    ts = pd.DataFrame(data.loc[ts_num])
    ts.columns = ["defo"]

    try:
        try:
            model = ARIMA(ts.defo, order=(1, 1, 1))
            model_fit = model.fit(disp=False)
            yhat = model_fit.predict(1, len(ts.defo) - 1)
            yhat = model_fit.predict(1, len(ts.defo) - 1, typ='levels')
            print('1,1,1')
        except ValueError:
            try:
                model = ARIMA(ts.defo, order=(2, 1, 1))
                model_fit = model.fit(disp=False)
                yhat = model_fit.predict(1, len(ts.defo) - 1)
                yhat = model_fit.predict(1, len(ts.defo) - 1, typ='levels')
                print('2,1,1')
            except ValueError:
                print('I couldn\'t do it :(')
                pass
    except np.linalg.LinAlgError:
        print('I suck at computing SVDs, I pass')
        pass

    if yhat is not None:
        ts['pred'] = yhat
        ts['offset'] = np.abs(ts.defo - ts['pred'])
        ts['unwrap'] = ts.offset > 0.75 * 18.1

        result_ARIMA = np.where(ts.unwrap.values)[0]

    else:
        result_ARIMA = []

    print(ts_num)
    return list(result_ARIMA)


class SimpleExpSmooth:
    def __init__(self, sl):
        self.sl = sl

    def fun(self, ts_num, data):
        ts = pd.DataFrame(data.iloc[ts_num])
        ts.columns = ["defo"]

        model1 = SimpleExpSmoothing(np.asarray(ts.defo))
        fit1 = model1.fit(smoothing_level=self.sl)
        ts['SES'] = pd.Series(fit1.predict(start=1, end=len(ts.defo)), index=ts.index).shift()
        # ts['SES2'] = ts.defo.ewm(com=0.3).mean().shift()
        ts['offset'] = np.abs(ts.defo - ts['SES'])
        ts['unwrap'] = ts.offset > 0.75 * 18.1

        #     ts['SES_std'] = ts.defo.rolling(5).std().shift()
        #     ts['unwrap_std'] = ts.offset + 2*ts['SES_std'] > 18.1 * 0.75

        result_SES = np.where(ts.unwrap.values)[0]
        return list(result_SES)


class DoubleExpSmooth:
    def __init__(self, sl, ss):
        self.sl = sl
        self.ss = ss

    def fun(self, ts_num, data):
        ts = pd.DataFrame(data.iloc[ts_num])
        ts.columns = ["defo"]

        model2 = ExponentialSmoothing(np.asarray(ts.defo), trend='add', damped=True)
        fit2 = model2.fit(smoothing_level=self.sl, smoothing_slope=self.ss)
        ts['DES'] = pd.Series(fit2.predict(start=1, end=len(ts.defo)), index=ts.index).shift()
        # ts['SES2'] = ts.defo.ewm(com=0.3).mean().shift()
        ts['offset'] = np.abs(ts.defo - ts['DES'])
        ts['unwrap'] = ts.offset > 0.75 * 18.1

        #     ts['SES_std'] = ts.defo.rolling(5).std().shift()
        #     ts['unwrap_std'] = ts.offset + 2*ts['SES_std'] > 18.1 * 0.75

        result_DES = np.where(ts.unwrap.values)[0]
        return list(result_DES)


class TripleExpSmooth:
    def __init__(self, sl, ss, sse):
        self.sl = sl
        self.ss = ss
        self.sse = sse

    def fun(self, ts_num, data):
        ts = pd.DataFrame(data.iloc[ts_num])
        ts.columns = ["defo"]

        model3 = ExponentialSmoothing(np.asarray(ts.defo), trend='add', seasonal='add', seasonal_periods=33)
        fit3 = model3.fit(smoothing_level=self.sl, smoothing_slope=self.ss, smoothing_seasonal=self.sse)
        ts['TES'] = pd.Series(fit3.predict(start=1, end=len(ts.defo)), index=ts.index).shift()
        # ts['SES2'] = ts.defo.ewm(com=0.3).mean().shift()
        ts['offset'] = np.abs(ts.defo - ts['TES'])
        ts['unwrap'] = ts.offset > 0.75 * 18.1

        #     ts['SES_std'] = ts.defo.rolling(5).std().shift()
        #     ts['unwrap_std'] = ts.offset + 2*ts['SES_std'] > 18.1 * 0.75

        result_TES = np.where(ts.unwrap.values)[0]
        return list(result_TES)


class MovAverage:
    def __init__(self, ue_thresh, window):
        self.ue_thresh = ue_thresh
        self.window = window

    def fun(self, ts_num, data):
        ts = pd.DataFrame(data.iloc[ts_num])
        ts.columns = ["defo"]

        ts['MA'] = ts.defo.rolling(self.window).mean().shift()
        #     ts['MA_centered'] = ts.defo.rolling(5, center = True).mean() # takes into account the value you want to predict
        ts['offset'] = np.abs(ts.defo - ts['MA'])
        #     ts['offset_centered'] = np.abs(ts.defo - ts['MA_centered'])
        ts['unwrap'] = ts.offset > self.ue_thresh * 18.1

        #     ts['MA_std'] = ts.defo.rolling(5).std().shift()
        #     ts['unwrap_std'] = ts.offset + 1*ts['MA_std'] > 18.1 * 0.75

        #         weights = np.array([0.1, 0.2, 0.3, 0.3, 0.1])

        #         def f(w):
        #             def g(x):
        #                 return (np.dot(w,x))
        #             return g

        #         ts['WMA'] = ts.defo.rolling(5).apply(f(weights)).shift()
        #         ts['offset_WMA'] = np.abs(ts.defo - ts['WMA'])
        #         ts['unwrap'] = ts.offset_WMA > 0.75*18.1

        result_MA = np.where(ts.unwrap.values)[0]
        return list(result_MA)


class NaiveApproach:
    def __init__(self, ue_thresh, window, method):
        self.ue_thresh = ue_thresh
        self.window = window
        self.method = method

    def fun(self, ts_num, data):
        #     ts = pd.DataFrame(mydata.iloc[ts_num,:])
        ts = pd.DataFrame(data.loc[ts_num])
        ts.columns = ["defo"]

        out = []
        threshold = []
        threshold2 = []
        mas = []
        mbs = []

        a = 0

        #     # 4 samples, mean
        if self.window == 4 and self.method == 'mean':
            for i in range(4, data.loc[ts_num].shape[0] - 4):
                #     yn = data.iloc[ts_num,:].diff().values[1:]
                yn = data.loc[ts_num]
                #     yn = (y - y.min())/(y.max()-y.min())
                ma = np.mean([yn[i - 3], yn[i - 2], yn[i - 4]])
                mb = np.mean([yn[i + 4], yn[i + 2], yn[i + 3]])
                thres = np.mean([(ma), (mb)]) + self.ue_thresh * 18.1
                thres2 = np.mean([(ma), (mb)]) - self.ue_thresh * 18.1
                o = (yn[i] >= thres - a or yn[i] <= thres2 + a)
                out.append(o)
                threshold.append(thres)
                threshold2.append(thres2)
                mas.append(ma)
                mbs.append(mb)

        #     # 4 samples, median
        elif self.window == 4 and self.method == 'median':
            for i in range(4, data.loc[ts_num].shape[0]-4):
            #     yn = data.iloc[ts_num,:].diff().values[1:]
                yn = data.loc[ts_num]
            #     yn = (y - y.min())/(y.max()-y.min())
                ma = np.median([yn[i-3], yn[i-2], yn[i-4]])
                mb = np.median([yn[i+4], yn[i+2], yn[i+3]])
                thres = np.mean([(ma), (mb)]) + self.ue_thresh * 18.1
                thres2 = np.mean([(ma), (mb)]) - self.ue_thresh * 18.1
                o = (yn[i] >= thres - a or yn[i] <= thres2 + a)
                out.append(o)
                threshold.append(thres)
                threshold2.append(thres2)
                mas.append(ma)
                mbs.append(mb)

        # 3 samples, mean
        elif self.window == 3 and self.method == 'mean':
            for i in range(3, data.loc[ts_num].shape[0]-3):
            #     yn = data.iloc[ts_num,:].diff().values[1:]
                yn = data.loc[ts_num]
            #     yn = (y - y.min())/(y.max()-y.min())
                ma = np.mean([yn[i-3], yn[i-2], yn[i-1]])
                mb = np.mean([yn[i+1], yn[i+2], yn[i+3]])
                thres = np.mean([(ma), (mb)]) + self.ue_thresh * 18.1
                thres2 = np.mean([(ma), (mb)]) - self.ue_thresh * 18.1
                o = (yn[i] >= thres - a or yn[i] <= thres2 + a)
                out.append(o)
                threshold.append(thres)
                threshold2.append(thres2)
                mas.append(ma)
                mbs.append(mb)

        # 3 samples, median
        elif self.window == 3 and self.method == 'mean':
            for i in range(3, data.loc[ts_num].shape[0]-3):
            #     yn = data.iloc[ts_num,:].diff().values[1:]
                yn = data.loc[ts_num]
            #     yn = (y - y.min())/(y.max()-y.min())
                ma = np.median([yn[i-3], yn[i-2], yn[i-1]])
                mb = np.median([yn[i+1], yn[i+2], yn[i+3]])
                thres = np.mean([(ma), (mb)]) + self.ue_thresh * 18.1
                thres2 = np.mean([(ma), (mb)]) - self.ue_thresh * 18.1
                o = (yn[i] >= thres - a or yn[i] <= thres2 + a)
                out.append(o)
                threshold.append(thres)
                threshold2.append(thres2)
                mas.append(ma)
                mbs.append(mb)

        if self.window == 3:
            result_naive = np.where(out)[0] + 3
        elif self.window == 4:
            result_naive = np.where(out)[0] + 4
        return list(result_naive)


