import pandas as pd
import numpy as np

def Testing(fun, mydata, ans, prints):
    
    full = set(range(0, mydata.iloc[0].shape[0]))
    nums = []
    results_pred_all = []

    Ss = []
    Ps = []
    ACCs = []
    MRs = []
    FOs = []
    FDRs = []
    FORs = []
    TPss = []
    FPss = []
    FNss = []
    TNss = []

    for i, p in enumerate(ans.points):
        ts_num = ans.index[i]
#         print('ts_num in testing:', ts_num)
#         print('mydata shape:', mydata.shape)
#         print('mydata index:', mydata.index)
        result_pred = fun(ts_num, mydata)
        result_gui = p 
        
        if prints:
            print('Time series #' + str(ts_num))
            print('Predicted:', result_pred)
            print('Ground truth:', result_gui)

        A = set(result_pred)
        B = set(result_gui)

        FPs = A - B
        FNs = B - A
        TPs = A & B
        TNs = (full - A) & (full - B)

        TP = len(TPs)
        FP = len(FPs)
        TN = len(TNs)
        FN = len(FNs)

        # Sensitivity
        if TP or FN:
            S = TP / (TP + FN)
        else:
            S = None

        # Precision
        if TP or FP:
            P = TP / (TP + FP)
        else:
            P = None

        # Accuracy
        ACC = (TP + TN) / (TP + TN + FP + FN)

        # Miss-rate
        if FN or TP:
            MR = FN / (FN + TP)
        else:
            MR = None

        # Fall-out
        FO = FP / (FP + TN)

        # False discovery rate
        if TP or FP:
            FDR = FP / (FP + TP)
        else:
            FDR = None

        # False omission rate
        FOR = FN / (FN + TN)

        if prints:
            print('A:', A, 'B:', B, 'FP:', FPs, 'FN:', FNs, 'TP:', TPs)# 'TN:', TN)
            print('Confusion matrix: \n', np.array([[TP, FP],[FN, TN]]))
            print("Sensitivity: ", S)
            print("Precision: ", P)
            print("Accuracy: ", ACC)
            print("Miss-rate: ", MR)
            print("Fall-out: ", FO)
            print("False discovery rate: ", FDR)
            print("False omission rate: ", FOR)
            print()
            print()

        nums.append(ts_num)
        results_pred_all.append(result_pred)

        Ss.append(S)
        Ps.append(P)
        ACCs.append(ACC)
        MRs.append(MR)
        FOs.append(FO)
        FDRs.append(FDR)
        FORs.append(FOR)
        TPss.append(TP)
        FPss.append(FP)
        FNss.append(FN)
        TNss.append(TN)
        
    metrics = pd.DataFrame(np.column_stack([Ss, Ps, ACCs, MRs, FOs, FDRs, FORs, TPss, FPss, FNss, TNss]), index = ans.index, columns = ['S', 'P', 'ACC', 'MR', 'FO', 'FDR', 'FOR', 'TP', 'FP', 'FN', 'TN'])
        
    return metrics, results_pred_all