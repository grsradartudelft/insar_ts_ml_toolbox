import numpy as np


def seg_line_sup(detected_lines, scores, min_distance=5):
    '''
    Segmentation Line Suppression
    Deletes detected segmentation lines if they are too close to each other.
    The lines with highest scores are prioritized to be kept
    
    Arguments:
    detected_lines - an array with the detected segmentation lines numbers
    scores - an array with scores corresponding to detected lines from detected_lines
    min_distance - minimum distance between segmentation lines (default =5)
    
    Returns:
    slines - an array with segmentation lines that are at least min_distance apart
    '''

    if not detected_lines:
        return [] 

    detected_lines = np.append(detected_lines, detected_lines[-1] + min_distance + 1)
    lines_diff = np.diff(detected_lines)
    
    slines = []    
    area_to_filter = []
    area_to_filter_scores = []
    
    for i in range(len(lines_diff)):
        if not area_to_filter:
            area_to_filter.append(detected_lines[i])
            area_to_filter_scores.append(scores[i])
            
        if lines_diff[i] < min_distance:
            area_to_filter.append(detected_lines[i+1])
            area_to_filter_scores.append(scores[i+1])
        else:
            if len(area_to_filter) == 1:
                slines.append(area_to_filter[0])
                area_to_filter = []
                area_to_filter_scores = []
            else:
                next_max_score_idx = 0
                sorted_area_to_filter_idx = sorted(range(len(area_to_filter_scores)), key=lambda k: area_to_filter_scores[k], reverse=True) 
                sorted_area_to_filter_idx_old = []
                to_del = []
                while sorted_area_to_filter_idx != sorted_area_to_filter_idx_old and next_max_score_idx < len(area_to_filter):
                    for j in range(len(area_to_filter)):
                        if abs(area_to_filter[sorted_area_to_filter_idx[next_max_score_idx]] - area_to_filter[j]) < min_distance \
                        and sorted_area_to_filter_idx[next_max_score_idx] != j:
                            to_del.append(j)
                    temp_area = []
                    temp_scores = []
                    for j, _ in enumerate(area_to_filter):
                        if j not in to_del:
                            temp_area.append(area_to_filter[j])
                            temp_scores.append(area_to_filter_scores[j])
                    area_to_filter = temp_area
                    area_to_filter_scores = temp_scores
                    sorted_area_to_filter_idx_old = sorted_area_to_filter_idx
                    sorted_area_to_filter_idx = sorted(range(len(area_to_filter_scores)), key=lambda k: area_to_filter_scores[k], reverse=True) 
                    next_max_score_idx = next_max_score_idx + 1
                    to_del = []
                    
                slines.extend(area_to_filter)    
                area_to_filter = []
                area_to_filter_scores = []

    return slines