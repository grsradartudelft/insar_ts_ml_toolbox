# InSAR Time Series analysis by Machine Learning

The InSAR Time Series Machine Learning toolbox provides tools for the analysis of InSAR Time Series by Machine Learning. The implemented methodologies are described in the MSc thesis of Ada Kazmierczak, see http://resolver.tudelft.nl/uuid:58ba73cd-28fc-4bfa-974c-d2406f29b383

The toolbox is mainly based on Python notebooks, enabling an interactive use. See the notebooks for further instructions.

